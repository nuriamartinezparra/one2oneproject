package com.example.one2oneproject.data.network.model.consultbeerlist

class IngredientsDataOut(
    val malt: ArrayList<Ingredient>,
    val hops: ArrayList<Ingredient>
)