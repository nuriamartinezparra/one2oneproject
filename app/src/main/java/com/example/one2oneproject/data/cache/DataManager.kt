package com.example.one2oneproject.data.cache

import com.example.one2oneproject.domain.bm.BeerItemBM

interface DataManager {
    fun setBeerListBM(listBM: List<BeerItemBM>)
    fun getBeerListBM(): List<BeerItemBM>
}