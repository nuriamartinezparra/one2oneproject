package com.example.one2oneproject.data.cache

import com.example.one2oneproject.domain.bm.BeerItemBM

class DataManagerImpl : DataManager {

    private lateinit var listBM: List<BeerItemBM>

    override fun setBeerListBM(listBM: List<BeerItemBM>) {
        this.listBM = listBM
    }

    override fun getBeerListBM(): List<BeerItemBM> {
        return this.listBM
    }
}