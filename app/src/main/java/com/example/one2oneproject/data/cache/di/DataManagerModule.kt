package com.example.one2oneproject.data.cache.di

import com.example.one2oneproject.data.cache.DataManager
import com.example.one2oneproject.data.cache.DataManagerImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataManagerModule {

    @Provides
    @Singleton
    fun provideDataManager(): DataManager = DataManagerImpl()
}