package com.example.one2oneproject.data.network.service

import com.example.one2oneproject.data.network.model.consultbeerlist.BeerItemDataOut
import com.example.one2oneproject.data.network.rest.RetrofitService
import com.example.one2oneproject.domain.repository.GetBeerListRepository
import io.reactivex.Single

class GetBeerListService: GetBeerListRepository {

    private val retrofit = RetrofitService.getRetrofit()

    override fun getBeersList(): Single<List<BeerItemDataOut>> {
        return (this.retrofit as GetBeerListRepository).getBeersList()
    }

}