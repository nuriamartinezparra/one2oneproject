package com.example.one2oneproject.data.network.model.consultbeerlist

import com.example.one2oneproject.data.network.model.base.BaseDataOut
import com.google.gson.annotations.SerializedName

class BeerItemDataOut(
    val id: Int,
    val name: String,
    val tagline: String,
    @SerializedName("first_brewed")
    val firstBrewed: String,
    val description: String,
    @SerializedName("image_url")
    val imageUrl: String,
    val abv: Double,
    val ibu: Double,
    @SerializedName("target_fg")
    val targetFg: Double,
    @SerializedName("target_og")
    val targetOg: Double,
    val ebc: Double,
    val srm: Double,
    val ph: Double,
    @SerializedName("attenuation_level")
    val attenuationLevel: Double,
    val ingredients: IngredientsDataOut,
    @SerializedName("brewers_tips")
    val brewersTips: String,
    @SerializedName("contributed_by")
    val contributedBy: String
): BaseDataOut()