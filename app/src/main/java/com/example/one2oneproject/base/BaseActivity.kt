package com.example.one2oneproject.base

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity: AppCompatActivity() {

    protected abstract fun inject() //Inject activity

    protected abstract fun afterInject() //Inflate binding

    protected abstract fun configureUI() //Initialize the binding elements of the view

    protected abstract fun setListeners() //Set onClickListeners

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(this.javaClass.simpleName,"Calling onCreate")
        this.inject()
        this.afterInject()
        this.configureUI()
        this.setListeners()
    }
}