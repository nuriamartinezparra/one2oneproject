package com.example.one2oneproject.base

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import com.example.one2oneproject.domain.bm.base.BaseBM
import com.example.one2oneproject.utils.Constants
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

abstract class BaseViewModel {

    private var getViewModelLiveData: MutableLiveData<BaseBM> = MutableLiveData()

    @SuppressLint("CheckResult")
    fun execute(useCase: Single<BaseBM>) {
        useCase.subscribeOn(Schedulers.io())
            .doOnSubscribe {
                //At this point we can show a loader
            }.observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                //Control of possible service errors
                throw Exception()
            }.subscribe { entity ->
                //At this point we must hide the loader
                getViewModelLiveData.value = entity
            }
    }

    fun getViewModelLiveData(): MutableLiveData<BaseBM> {
        return getViewModelLiveData
    }

    fun navigateWithBM(
        context: Context,
        intent: Intent,
        id: String
    ) {
        intent.putExtra(Constants.BEER_ID, id)
        (context as Activity).startActivity(intent)
    }
}