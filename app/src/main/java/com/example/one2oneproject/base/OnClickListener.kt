package com.example.one2oneproject.base

import com.example.one2oneproject.domain.bm.BeerItemBM

interface OnClickListener {
    fun onItemClick(itemBM: BeerItemBM)
}