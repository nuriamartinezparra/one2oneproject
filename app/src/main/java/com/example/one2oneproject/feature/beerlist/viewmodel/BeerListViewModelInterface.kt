package com.example.one2oneproject.feature.beerlist.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.one2oneproject.domain.bm.base.BaseBM

interface BeerListViewModelInterface{
    fun getBeersList()
    fun getBeerListObserver(): MutableLiveData<BaseBM>
    fun navigateToBeerDetail(context: Context, id: String)
}