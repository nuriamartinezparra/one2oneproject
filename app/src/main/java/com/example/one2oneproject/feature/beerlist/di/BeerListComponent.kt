package com.example.one2oneproject.feature.beerlist.di

import com.example.one2oneproject.feature.beerlist.BeerListActivity
import dagger.Subcomponent

@Subcomponent(modules = [BeerListModule::class])
interface BeerListComponent {
    fun inject(beerListActivity: BeerListActivity)
}