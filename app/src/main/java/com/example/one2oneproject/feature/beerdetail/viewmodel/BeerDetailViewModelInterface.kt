package com.example.one2oneproject.feature.beerdetail.viewmodel

import com.example.one2oneproject.domain.bm.BeerItemBM

interface BeerDetailViewModelInterface {
    fun getBeerInfo(id: String): BeerItemBM?
}