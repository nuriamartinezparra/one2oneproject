package com.example.one2oneproject.feature.beerdetail.di

import com.example.one2oneproject.data.cache.DataManager
import com.example.one2oneproject.data.network.service.GetBeerListService
import com.example.one2oneproject.domain.repository.GetBeerListRepository
import com.example.one2oneproject.domain.usecase.GetBeerListUseCase
import com.example.one2oneproject.domain.usecase.GetBeerListUseCaseImpl
import com.example.one2oneproject.feature.beerdetail.viewmodel.BeerDetailViewModel
import com.example.one2oneproject.feature.beerdetail.viewmodel.BeerDetailViewModelInterface
import dagger.Module
import dagger.Provides

@Module
class BeerDetailModule {

    @Provides
    fun provideBeerDetailViewModel(useCase: GetBeerListUseCase): BeerDetailViewModelInterface =
        BeerDetailViewModel(useCase)

    @Provides
    fun provideGetBeerListUseCase(
        service: GetBeerListRepository,
        dataManager: DataManager
    ): GetBeerListUseCase = GetBeerListUseCaseImpl(service, dataManager)

    @Provides
    fun provideGetBeerListService(): GetBeerListRepository = GetBeerListService()

}