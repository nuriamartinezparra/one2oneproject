package com.example.one2oneproject.feature.beerlist

import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.one2oneproject.ProjectApplication
import com.example.one2oneproject.base.BaseActivity
import com.example.one2oneproject.base.OnClickListener
import com.example.one2oneproject.databinding.ActivityBeerListBinding
import com.example.one2oneproject.domain.bm.BeerItemBM
import com.example.one2oneproject.domain.bm.BeerListBM
import com.example.one2oneproject.feature.beerlist.viewmodel.BeerListViewModelInterface
import javax.inject.Inject

class BeerListActivity : BaseActivity(), OnClickListener {

    private lateinit var binding: ActivityBeerListBinding
    private lateinit var listBM: List<BeerItemBM>
    private lateinit var filterListBM: List<BeerItemBM>

    @Inject
    lateinit var viewModel: BeerListViewModelInterface

    override fun inject() {
        /*Inject component*/
        ProjectApplication.get(this).getApplicationComponent().getBeerListComponent().inject(this)
    }

    override fun afterInject() {
        /*Set binding to content view*/
        this.binding = ActivityBeerListBinding.inflate(this.layoutInflater)
        this.setContentView(this.binding.root)
        /*Initialize observers*/
        this.setObservers()
        /*Call beers list service to create list*/
        this.callService()

    }

    override fun configureUI() {
        /*Initialize the view components*/
    }

    override fun setListeners() {
        /*Initialize the click listeners*/
        this.binding.filterTextInput.editText?.addTextChangedListener {
            //Filter list
            this.setBeerList(this.filterList(this.binding.filterTextInput.editText?.text.toString()))
        }
        this.binding.btnCleanTextInput.setOnClickListener {
            //Clean text input
            this.binding.filterTextInput.editText?.text = null
            this.setBeerList(this.listBM)
        }
        this.binding.iconRefresh.setOnClickListener {
            //Call beers list service to refresh list
            this.callService()
            this.binding.filterTextInput.editText?.text = null
        }
    }

    private fun callService() {
        /*Call service for get beers list*/
        this.viewModel.getBeersList()
    }

    private fun setObservers() {
        /*Get observer*/
        viewModel.getBeerListObserver().observe(this) { bm ->
            with(bm as BeerListBM){
                this@BeerListActivity.listBM = this.getList()
                this@BeerListActivity.setBeerList(this.getList())
            }
        }
    }

    private fun setBeerList(list: List<BeerItemBM>){
        /*Set the beers list*/
        if (list.isNotEmpty()){
            //Hide no have beers
            this.binding.noBeersAvailable.visibility = View.GONE
            this.binding.beerListRecyclerView.visibility = View.VISIBLE
            //Create recycler view with the beers list
            this.binding.beerListRecyclerView.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = BeerListAdapter(list, this@BeerListActivity)
            }

        } else {
            //Show no have beers
            this.binding.beerListRecyclerView.visibility = View.GONE
            this.binding.noBeersAvailable.visibility = View.VISIBLE
        }
    }

    private fun filterList(filter: String?): List<BeerItemBM> {
        /*Filter list*/
        this.filterListBM = this.listBM
        filter?.let { filter ->
            //Set beer item that contain the filter
            this.filterListBM =
                this.listBM.filter { it.getName().contains(filter, ignoreCase = true) }
        }
        //Update view with filter list
        return this.filterListBM
    }

    override fun onItemClick(itemBM: BeerItemBM) {
        this.viewModel.navigateToBeerDetail(this, itemBM.getId().toString())
    }

}