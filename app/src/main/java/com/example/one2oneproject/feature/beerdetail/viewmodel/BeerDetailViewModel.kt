package com.example.one2oneproject.feature.beerdetail.viewmodel

import com.example.one2oneproject.base.BaseViewModel
import com.example.one2oneproject.domain.bm.BeerItemBM
import com.example.one2oneproject.domain.usecase.GetBeerListUseCase
import javax.inject.Inject

class BeerDetailViewModel @Inject constructor(
    private var getBeersUseCase: GetBeerListUseCase
) : BaseViewModel(), BeerDetailViewModelInterface {
    override fun getBeerInfo(id: String): BeerItemBM? {
        return getBeersUseCase.getBeerInfo(id)
    }

}