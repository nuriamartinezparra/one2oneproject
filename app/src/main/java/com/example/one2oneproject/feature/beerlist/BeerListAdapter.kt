package com.example.one2oneproject.feature.beerlist

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.one2oneproject.R
import com.example.one2oneproject.base.OnClickListener
import com.example.one2oneproject.databinding.ComponentBeerItemBinding
import com.example.one2oneproject.domain.bm.BeerItemBM

class BeerListAdapter(private var list: List<BeerItemBM>, private var listener: OnClickListener) :
    RecyclerView.Adapter<BeerListAdapter.ViewHolder>() {

    private lateinit var binding: ComponentBeerItemBinding

    class ViewHolder(private var context: Context, private val binding: ComponentBeerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(itemBM: BeerItemBM, listener: OnClickListener) {
            Glide.with(context).load(itemBM.getImageUrl()).into(this.binding.imageBeerItem)
            this.binding.nameBeerItem.text = itemBM.getName()
            this.binding.firstBrewedBeerItem.text =
                context.resources.getString(R.string.first_brewed_text) + " " + itemBM.getFirstBrewed()
            this.binding.degreesBeerItem.text = "Alc. " + itemBM.getDegrees() + "% vol."
            this.binding.root.setOnClickListener {
                listener.onItemClick(itemBM)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        this.binding = ComponentBeerItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(parent.context, this.binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemBM = this.list[position]
        holder.bind(itemBM, this.listener)
    }

    override fun getItemCount(): Int = this.list.size

}