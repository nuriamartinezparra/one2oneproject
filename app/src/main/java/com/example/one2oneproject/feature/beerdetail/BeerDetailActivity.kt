package com.example.one2oneproject.feature.beerdetail

import android.annotation.SuppressLint
import com.bumptech.glide.Glide
import com.example.one2oneproject.ProjectApplication
import com.example.one2oneproject.base.BaseActivity
import com.example.one2oneproject.databinding.ActivityBeerDetailBinding
import com.example.one2oneproject.domain.bm.BeerItemBM
import com.example.one2oneproject.feature.beerdetail.viewmodel.BeerDetailViewModelInterface
import com.example.one2oneproject.utils.Constants.Companion.BEER_ID
import javax.inject.Inject

class BeerDetailActivity : BaseActivity() {

    private lateinit var binding: ActivityBeerDetailBinding
    private var itemBM: BeerItemBM? = null

    @Inject
    lateinit var viewModel: BeerDetailViewModelInterface

    override fun inject() {
        /*Inject component*/
        ProjectApplication.get(this).getApplicationComponent().getBeerDetailComponent().inject(this)
    }

    override fun afterInject() {
        /*Set binding to content view*/
        this.binding = ActivityBeerDetailBinding.inflate(this.layoutInflater)
        this.setContentView(this.binding.root)
        //Get beer info
        val id = intent.getStringExtra(BEER_ID)
        id?.let {
            this.getBeerInfo(it)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun configureUI() {
        /*Initialize the view components*/
        this.itemBM?.let {
            Glide.with(this).load(it.getImageUrl()).into(this.binding.imageBeerDetail)
            this.binding.nameBeerDetail.text = it.getName()
            this.binding.tagBeerDetail.text = it.getTagline()
            this.binding.descriptionBeerDetail.text = it.getDescription()
            this.binding.valueDegreesBeerDetail.text = it.getDegrees() + "º"
            this.binding.valuePhBeerDetail.text = it.getPH()
            this.binding.valueIngredientsBeerDetail.text =
                this.getIngredientsString(it.getIngredients())
            this.binding.valueTipsBeerDetail.text = it.getBrewersTips()
            this.binding.valueContributedBeerDetail.text = it.getContributedBy()
        }
    }

    override fun setListeners() {
        /*Initialize the click listeners*/
        this.binding.iconBackBeerDetail.setOnClickListener {
            this.finish()
        }
    }

    private fun getBeerInfo(idBeer: String) {
        this.itemBM = this.viewModel.getBeerInfo(idBeer)
    }

    private fun getIngredientsString(ingredients: ArrayList<String>): String {
        var stgIngredient = ""
        for (item in ingredients) {
            stgIngredient += "$item, "
        }
        return stgIngredient
    }

}