package com.example.one2oneproject.feature.beerlist.di

import com.example.one2oneproject.data.cache.DataManager
import com.example.one2oneproject.data.network.service.GetBeerListService
import com.example.one2oneproject.domain.repository.GetBeerListRepository
import com.example.one2oneproject.domain.usecase.GetBeerListUseCase
import com.example.one2oneproject.domain.usecase.GetBeerListUseCaseImpl
import com.example.one2oneproject.feature.beerlist.viewmodel.BeerListViewModel
import com.example.one2oneproject.feature.beerlist.viewmodel.BeerListViewModelInterface
import dagger.Module
import dagger.Provides

@Module
class BeerListModule {

    @Provides
    fun provideBeerListViewModel(useCase: GetBeerListUseCase): BeerListViewModelInterface =
        BeerListViewModel(useCase)

    @Provides
    fun provideGetBeerListUseCase(
        service: GetBeerListRepository,
        dataManager: DataManager
    ): GetBeerListUseCase = GetBeerListUseCaseImpl(service, dataManager)

    @Provides
    fun provideGetBeerListService(): GetBeerListRepository = GetBeerListService()

}