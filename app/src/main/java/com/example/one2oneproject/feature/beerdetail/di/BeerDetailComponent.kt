package com.example.one2oneproject.feature.beerdetail.di

import com.example.one2oneproject.feature.beerdetail.BeerDetailActivity
import dagger.Subcomponent

@Subcomponent(modules = [BeerDetailModule::class])
interface BeerDetailComponent {
    fun inject(beerDetailActivity: BeerDetailActivity)
}