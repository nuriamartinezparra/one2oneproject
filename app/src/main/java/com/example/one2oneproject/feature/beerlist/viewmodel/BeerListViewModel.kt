package com.example.one2oneproject.feature.beerlist.viewmodel

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import com.example.one2oneproject.base.BaseViewModel
import com.example.one2oneproject.domain.bm.base.BaseBM
import com.example.one2oneproject.domain.usecase.GetBeerListUseCase
import com.example.one2oneproject.feature.beerdetail.BeerDetailActivity
import javax.inject.Inject

class BeerListViewModel@Inject constructor(
    private var getBeersUseCase: GetBeerListUseCase
): BaseViewModel(), BeerListViewModelInterface {

    @SuppressLint("CheckResult")
    override fun getBeersList() {
        /*Call the execute of the base*/
        super.execute(getBeersUseCase.getBeers())
    }

    override fun getBeerListObserver(): MutableLiveData<BaseBM> {
        /*Get observer of the base and return this*/
        return super.getViewModelLiveData()
    }

    override fun navigateToBeerDetail(context: Context, id: String) {
        /*Navigate to beer detail with BM*/
        val intent = Intent(context, BeerDetailActivity::class.java)
        super.navigateWithBM(context, intent, id)
    }

}