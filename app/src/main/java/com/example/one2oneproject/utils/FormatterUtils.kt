package com.example.one2oneproject.utils

object FormatterUtils {
    
    /**
     * Date to API input format.
     *
     * @param date MM/yyyy
     * @return Mes yyyy
     */
    fun formatDate(date: String): String {
        if(date.contains("/")){
            val month = date.split("/")[0]
            val year = date.split("/")[1]
            var monthName = ""
            for(item in Constants.MONTH_MAP){
                if(item.key == month){
                    monthName = item.value
                }
            }
            return "$monthName $year"
        }
        return date
    }

    /**
     * Date to API input format.
     *
     * @param String 0.0
     * @return 0,0
     */
    fun formatNumber(number: String): String {
        if(number.contains(".")){
            return number.replace(".", ",")
        }
        return number
    }

}