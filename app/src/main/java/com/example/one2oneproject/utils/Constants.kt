package com.example.one2oneproject.utils

interface Constants {
    companion object {

        const val BEER_ID = "id"

        val MONTH_MAP: Map<String, String> = mapOf(
            "01" to "January",
            "02" to "February",
            "03" to "March",
            "04" to "April",
            "05" to "May",
            "06" to "June",
            "07" to "July",
            "08" to "August",
            "09" to "September",
            "10" to "October",
            "11" to "November",
            "12" to "December"
        )
    }
}