package com.example.one2oneproject.domain.bm

import com.example.one2oneproject.data.network.model.consultbeerlist.BeerItemDataOut
import com.example.one2oneproject.domain.bm.base.BaseBM

class BeerListBM: BaseBM() {

    private var beerListBM: List<BeerItemBM> = listOf()

    fun dataOutToEntity(dataOut: List<BeerItemDataOut>): BaseBM {
        /*Create BeerListBM and return this*/
        for (item in dataOut){
            //Create a list of BeerItemBM
            val itemBM = BeerItemBM().dataOutToEntity(item)
            this@BeerListBM.beerListBM = this@BeerListBM.beerListBM + listOf(itemBM)
        }
        return this
    }

    fun getList() = beerListBM
}