package com.example.one2oneproject.domain.bm

import com.example.one2oneproject.data.network.model.consultbeerlist.BeerItemDataOut
import com.example.one2oneproject.data.network.model.consultbeerlist.IngredientsDataOut
import com.example.one2oneproject.domain.bm.base.BaseBM
import com.example.one2oneproject.utils.FormatterUtils.formatDate
import com.example.one2oneproject.utils.FormatterUtils.formatNumber

class BeerItemBM: BaseBM() {

    private var id: Int = -1
    private lateinit var name: String
    private lateinit var firstBrewed: String
    private lateinit var imageUrl: String
    private lateinit var description: String
    private lateinit var degrees: String
    private lateinit var tagline: String
    private lateinit var ph: String
    private lateinit var brewersTips: String
    private lateinit var contributedBy: String
    private var ingredients: ArrayList<String> = arrayListOf()

    fun dataOutToEntity(dataOut: BeerItemDataOut): BeerItemBM {
        /*Create BeerItemBM and return this*/
        this.id = dataOut.id
        this.name = dataOut.name
        this.firstBrewed = formatDate(dataOut.firstBrewed)
        this.imageUrl = dataOut.imageUrl
        this.description = dataOut.description
        this.degrees = formatNumber(dataOut.abv.toString())
        this.tagline = dataOut.tagline
        this.ph = formatNumber(dataOut.ph.toString())
        this.brewersTips = dataOut.brewersTips
        this.contributedBy = dataOut.contributedBy
        this.getIngredientsBM(dataOut.ingredients)

        return this
    }

    private fun getIngredientsBM(ingredients: IngredientsDataOut) {
        for (ingredient in ingredients.malt) {
            this.ingredients += arrayListOf(ingredient.name)
        }
        for (ingredient in ingredients.hops) {
            this.ingredients += arrayListOf(ingredient.name)
        }
    }

    fun getId() = this.id

    fun getName() = this.name

    fun getFirstBrewed() = this.firstBrewed

    fun getImageUrl() = this.imageUrl

    fun getDescription() = this.description

    fun getDegrees() = this.degrees

    fun getTagline() = this.tagline

    fun getPH() = this.ph

    fun getBrewersTips() = this.brewersTips

    fun getContributedBy() = this.contributedBy

    fun getIngredients() = this.ingredients
}