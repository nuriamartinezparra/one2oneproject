package com.example.one2oneproject.domain.usecase

import com.example.one2oneproject.domain.bm.BeerItemBM
import com.example.one2oneproject.domain.bm.base.BaseBM
import com.example.one2oneproject.domain.usecase.base.BaseUseCase
import io.reactivex.Single

interface GetBeerListUseCase: BaseUseCase {
    fun getBeers(): Single<BaseBM>
    fun getBeerInfo(id: String): BeerItemBM?
}