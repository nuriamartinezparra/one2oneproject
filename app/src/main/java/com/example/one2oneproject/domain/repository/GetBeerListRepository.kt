package com.example.one2oneproject.domain.repository

import com.example.one2oneproject.data.network.model.consultbeerlist.BeerItemDataOut
import com.example.one2oneproject.domain.repository.base.BaseRepository
import io.reactivex.Single
import retrofit2.http.GET

interface GetBeerListRepository: BaseRepository {

    @GET("beers")
    fun getBeersList(): Single<List<BeerItemDataOut>>
}