package com.example.one2oneproject.domain.usecase

import com.example.one2oneproject.data.cache.DataManager
import com.example.one2oneproject.domain.bm.BeerItemBM
import com.example.one2oneproject.domain.bm.BeerListBM
import com.example.one2oneproject.domain.bm.base.BaseBM
import com.example.one2oneproject.domain.repository.GetBeerListRepository
import io.reactivex.Single
import javax.inject.Inject

class GetBeerListUseCaseImpl @Inject constructor(
    private val consultBeerListService: GetBeerListRepository,
    private val dataManager: DataManager
): GetBeerListUseCase {

    override fun getBeers(): Single<BaseBM> {
        val result = consultBeerListService.getBeersList()

        return result.map { data ->
            val dataResult = BeerListBM().dataOutToEntity(data)
            this.setBmIntoManager(dataResult)
            return@map dataResult
        }.doOnError { error ->
            print(error)
        }
    }

    override fun getBeerInfo(id: String): BeerItemBM? {
        for (itemBM in dataManager.getBeerListBM()) {
            if (id == itemBM.getId().toString()) {
                return itemBM
            }
        }
        return null
    }

    private fun setBmIntoManager(dataResult: BaseBM) {
        with(dataResult as BeerListBM) {
            dataManager.setBeerListBM(this.getList())
        }
    }
}