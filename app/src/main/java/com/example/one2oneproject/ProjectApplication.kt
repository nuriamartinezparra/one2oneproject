package com.example.one2oneproject

import android.app.Activity
import android.app.Application
import android.content.Context

class ProjectApplication: Application() {

    private lateinit var one2oneProjectComponent: ProjectComponent

    override fun onCreate() {
        super.onCreate()
        one2oneProjectComponent = DaggerProjectComponent.builder().context(applicationContext).build()
    }

    fun getApplicationComponent(): ProjectComponent {
        return one2oneProjectComponent
    }

    companion object {
        fun get(activity: Activity): ProjectApplication {
            return activity.application as ProjectApplication
        }

        fun get(context: Context): ProjectApplication {
            return context.applicationContext as ProjectApplication
        }
    }
}