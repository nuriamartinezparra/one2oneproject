package com.example.one2oneproject

import android.content.Context
import com.example.one2oneproject.data.cache.di.DataManagerModule
import com.example.one2oneproject.feature.beerdetail.di.BeerDetailComponent
import com.example.one2oneproject.feature.beerlist.di.BeerListComponent
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(modules = [DataManagerModule::class])
@Singleton
interface ProjectComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(context: Context): Builder
        fun build(): ProjectComponent
    }

    fun getBeerListComponent(): BeerListComponent
    fun getBeerDetailComponent(): BeerDetailComponent
}