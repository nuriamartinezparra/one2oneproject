# Introduction

This project contains a beer finder using the Punk API and Kotlin programming language.
[Documentation](https://punkapi.com/documentation/v2)

The architecture is based on the MVVM design pattern.

# Requirements

• The user must be able to insert the name of a beer in a search engine and obtain the search result
in a list.

• Each time the user enters or deletes a character from the search, the list must be updated.

# Additional

When clicking on a beer in the list, a new screen should open with the beer information (name,
image, description, degrees).

# Project objectives

Some of the objectives to be achieved are:

- create an efficient, robust and stable software that meets the requirements

- write clean and flexible code, which can adapt to changes easily

# Project settings

This project should be compatible with any version of Android Studio that supports Kotlin. Even so,
the project has been developed with Android Studio Bumblebee | 2021.1.1 Patch 3

The versions of Gradle that have been used are:

- Android Gradle Plugin Version v3.6.3
- Gradle Version v6.1.1

All libraries and dependencies used are configured in the file app/build.gradle and their respective
versions are in the file ../versions.gradle

Internet connection is required.

# Additional comments

It may happen that the list of beers is not loaded after not receiving service data. In that case
clean and recompile the project. It may even be necessary to close the Android emulator.